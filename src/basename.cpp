#include <stream9/path/basename.hpp>
#include <stream9/path/dirname.hpp>
#include <stream9/path/split.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

using path::basename;
using path::dirname;

BOOST_AUTO_TEST_SUITE(path_)

BOOST_AUTO_TEST_SUITE(basename_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto f = basename("");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(root_)
    {
        auto f = basename("/");

        BOOST_TEST(f == "/");
    }

    BOOST_AUTO_TEST_CASE(no_slash_)
    {
        auto f = basename("xxx");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_1_)
    {
        auto f = basename("xxx/");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_2_)
    {
        auto f = basename("xxx//");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_3_)
    {
        auto f = basename("///");

        BOOST_TEST(f == "/");
    }

    BOOST_AUTO_TEST_CASE(one_slash_2_)
    {
        auto f = basename("/xxx");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(one_slash_3_)
    {
        auto f = basename("yyy/xxx");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(two_slash_1_)
    {
        auto f = basename("/yyy/xxx");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(three_slash_1_)
    {
        auto f = basename("/yyy/xxx/");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(three_slash_2_)
    {
        auto f = basename("/yyy/zzz/xxx");

        BOOST_TEST(f == "xxx");
    }

    BOOST_AUTO_TEST_CASE(dot_1_)
    {
        auto f = basename(".");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(dot_2_)
    {
        auto f = basename("/usr/.");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(dot_dot_1_)
    {
        auto f = basename("..");

        BOOST_TEST(f == "..");
    }

    BOOST_AUTO_TEST_CASE(dot_dot_2_)
    {
        auto f = basename("/usr/../");

        BOOST_TEST(f == "..");
    }

BOOST_AUTO_TEST_SUITE_END() // basename_

BOOST_AUTO_TEST_SUITE(dirname_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto f = dirname("");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(root_)
    {
        auto f = dirname("/");

        BOOST_TEST(f == "/");
    }

    BOOST_AUTO_TEST_CASE(no_slash_)
    {
        auto f = dirname("xxx");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_1_)
    {
        auto f = dirname("xxx/");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_2_)
    {
        auto f = dirname("xxx//");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_3_)
    {
        auto f = dirname("///");

        BOOST_TEST(f == "/");
    }

    BOOST_AUTO_TEST_CASE(one_slash_2_)
    {
        auto f = dirname("/xxx");

        BOOST_TEST(f == "/");
    }

    BOOST_AUTO_TEST_CASE(one_slash_3_)
    {
        auto f = dirname("yyy/xxx");

        BOOST_TEST(f == "yyy");
    }

    BOOST_AUTO_TEST_CASE(two_slash_1_)
    {
        auto f = dirname("/yyy/xxx");

        BOOST_TEST(f == "/yyy");
    }

    BOOST_AUTO_TEST_CASE(three_slash_1_)
    {
        auto f = dirname("/yyy/xxx/");

        BOOST_TEST(f == "/yyy");
    }

    BOOST_AUTO_TEST_CASE(three_slash_2_)
    {
        auto f = dirname("/yyy/zzz/xxx");

        BOOST_TEST(f == "/yyy/zzz");
    }

    BOOST_AUTO_TEST_CASE(dot_1_)
    {
        auto f = dirname(".");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(dot_2_)
    {
        auto f = dirname("/usr/.");

        BOOST_TEST(f == "/usr");
    }

    BOOST_AUTO_TEST_CASE(dot_dot_1_)
    {
        auto f = dirname("..");

        BOOST_TEST(f == ".");
    }

    BOOST_AUTO_TEST_CASE(dot_dot_2_)
    {
        auto f = dirname("/usr/../");

        BOOST_TEST(f == "/usr");
    }

BOOST_AUTO_TEST_SUITE_END() // dirname_

BOOST_AUTO_TEST_SUITE(split_)

    using path::split;

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto [d, b] = split("");

        BOOST_TEST(d == ".");
        BOOST_TEST(b == ".");
    }

    BOOST_AUTO_TEST_CASE(root_)
    {
        auto [d, b] = split("/");

        BOOST_TEST(d == "/");
        BOOST_TEST(b == "/");
    }

    BOOST_AUTO_TEST_CASE(no_slash_)
    {
        auto [d, b] = split("xxx");

        BOOST_TEST(d == ".");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_1_)
    {
        auto [d, b] = split("xxx/");

        BOOST_TEST(d == ".");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_2_)
    {
        auto [d, b] = split("xxx//");

        BOOST_TEST(d == ".");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(trailing_slash_is_not_part_of_path_3_)
    {
        auto [d, b] = split("///");

        BOOST_TEST(d == "/");
        BOOST_TEST(b == "/");
    }

    BOOST_AUTO_TEST_CASE(one_slash_2_)
    {
        auto [d, b] = split("/xxx");

        BOOST_TEST(d == "/");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(one_slash_3_)
    {
        auto [d, b] = split("yyy/xxx");

        BOOST_TEST(d == "yyy");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(two_slash_1_)
    {
        auto [d, b] = split("/yyy/xxx");

        BOOST_TEST(d == "/yyy");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(three_slash_1_)
    {
        auto [d, b] = split("/yyy/xxx/");

        BOOST_TEST(d == "/yyy");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(three_slash_2_)
    {
        auto [d, b] = split("/yyy/zzz/xxx");

        BOOST_TEST(d == "/yyy/zzz");
        BOOST_TEST(b == "xxx");
    }

    BOOST_AUTO_TEST_CASE(dot_1_)
    {
        auto [d, b] = split(".");

        BOOST_TEST(d == ".");
        BOOST_TEST(b == ".");
    }

    BOOST_AUTO_TEST_CASE(dot_2_)
    {
        auto [d, b] = split("/usr/.");

        BOOST_TEST(d == "/usr");
        BOOST_TEST(b == ".");
    }

    BOOST_AUTO_TEST_CASE(dot_dot_1_)
    {
        auto [d, b] = split("..");

        BOOST_TEST(d == ".");
        BOOST_TEST(b == "..");
    }

    BOOST_AUTO_TEST_CASE(dot_dot_2_)
    {
        auto [d, b] = split("/usr/../");

        BOOST_TEST(d == "/usr");
        BOOST_TEST(b == "..");
    }

BOOST_AUTO_TEST_SUITE_END() // split_

BOOST_AUTO_TEST_SUITE_END() // path_

} // namespace testing
