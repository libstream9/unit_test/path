#include <stream9/path/is_child_of.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(is_child_of_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        BOOST_TEST(path::is_child_of("", ""));
        BOOST_TEST(path::is_child_of("foo", ""));
        BOOST_TEST(path::is_child_of("/foo", "/"));
        BOOST_TEST(path::is_child_of("foo", "foo/"));
        BOOST_TEST(path::is_child_of("foo/bar", "foo"));
        BOOST_TEST(path::is_child_of("foo/bar/", "foo"));
        BOOST_TEST(path::is_child_of("foo/bar", "foo/"));
        BOOST_TEST(path::is_child_of("foo/bar/", "foo/"));
        BOOST_TEST(!path::is_child_of("foo", "fo"));
    }

BOOST_AUTO_TEST_SUITE_END() // is_child_of_

} // namespace testing

