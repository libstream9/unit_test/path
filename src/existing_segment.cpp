#include <stream9/path/existing_segment.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(existing_segment_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto p = path::existing_segment("");

        BOOST_TEST(p == ".");
    }

    BOOST_AUTO_TEST_CASE(level_1_)
    {
        auto p = path::existing_segment("/foo");

        BOOST_TEST(p == "/");
    }

    BOOST_AUTO_TEST_CASE(level_2_)
    {
        auto p = path::existing_segment("/proc/self/xxx");

        BOOST_TEST(p == "/proc/self");
    }

BOOST_AUTO_TEST_SUITE_END() // existing_segment_

} // namespace testing
