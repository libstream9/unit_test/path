#ifndef STREAM9_PATH_TEST_SRC_NAMESPACE_HPP
#define STREAM9_PATH_TEST_SRC_NAMESPACE_HPP

namespace stream9::path {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace path { using namespace st9::path; }

} // namespace testing

#endif // STREAM9_PATH_TEST_SRC_NAMESPACE_HPP
