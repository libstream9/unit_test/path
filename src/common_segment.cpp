#include <stream9/path/common_segment.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(common_segment_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto s = path::common_segment("", "");

        BOOST_TEST(s == "");
    }

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        auto s = path::common_segment("foo", "foo/bar");

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        auto s = path::common_segment("/foo/bar", "/foo");

        BOOST_TEST(s == "/foo");
    }

    BOOST_AUTO_TEST_CASE(basic_3_)
    {
        auto s = path::common_segment("/foo/bar", "/baz");

        BOOST_TEST(s == "/");
    }

    BOOST_AUTO_TEST_CASE(no_segment_)
    {
        auto s = path::common_segment("foo/bar", "baz");

        BOOST_TEST(s == "");
    }

BOOST_AUTO_TEST_SUITE_END() // common_segment_

} // namespace testing
