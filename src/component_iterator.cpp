#include <stream9/path/component_iterator.hpp>
#include <stream9/path/components.hpp>

#include <iterator>

#include <boost/test/unit_test.hpp>

#include <stream9/array.hpp>
#include <stream9/json.hpp>
#include <stream9/string.hpp>

namespace testing {

using stream9::array;
using stream9::path::component_iterator;
using stream9::path::components;
using stream9::string_view;

namespace json = stream9::json;

BOOST_AUTO_TEST_SUITE(component_iterator_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        component_iterator i1 { "" };

        BOOST_TEST(*i1 == "");

        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        component_iterator i1 { "/" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "/");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        component_iterator i1 { "foo" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(basic_3_)
    {
        component_iterator i1 { "/foo" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "/");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(basic_4_)
    {
        component_iterator i1 { "/foo/bar" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "/");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "bar");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(basic_5_)
    {
        component_iterator i1 { "/foo/bar/" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "/");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "bar");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(basic_6_)
    {
        component_iterator i1 { "foo/bar" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "bar");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(basic_7_)
    {
        component_iterator i1 { "foo/bar/" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "bar");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(consecutive_slash_1_)
    {
        component_iterator i1 { "foo//bar" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "bar");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

    BOOST_AUTO_TEST_CASE(consecutive_slash_2_)
    {
        component_iterator i1 { "///foo///" };

        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "/");

        ++i1;
        BOOST_CHECK(i1 != std::default_sentinel);
        BOOST_TEST(*i1 == "foo");

        ++i1;
        BOOST_CHECK(i1 == std::default_sentinel);
    }

BOOST_AUTO_TEST_SUITE_END() // component_iterator_

BOOST_AUTO_TEST_SUITE(components_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        components c { "/foo/bar" };

        array<string_view> comp { c };
        array<string_view> expected { "/" ,"foo", "bar" };

        BOOST_TEST(comp == expected);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        components c { "" };

        array<string_view> comp { c };
        array<string_view> expected {};

        BOOST_TEST(comp == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // component_range_

} // namespace testing
