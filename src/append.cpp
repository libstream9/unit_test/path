#include <stream9/path/append.hpp>

#include "namespace.hpp"

#include <stream9/path/concat.hpp>
#include <stream9/string.hpp>
#include <concepts>
#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::string;

BOOST_AUTO_TEST_SUITE(path_)

    BOOST_AUTO_TEST_SUITE(append_)

        BOOST_AUTO_TEST_CASE(lvalue_string_to_string_view_)
        {
            using path::append;

            string p1 = "foo";

            using T = decltype(append(p1, "bar"));
            static_assert(std::same_as<T, string&>);

            BOOST_TEST(append(p1, "bar") == "foo/bar");
        }

        BOOST_AUTO_TEST_CASE(rvalue_string_to_string_view_)
        {
            using path::append;

            string p1 = "foo";

            using T = decltype(append(std::move(p1), "bar"));
            static_assert(std::same_as<T, string&&>);

            BOOST_TEST(append(std::move(p1), "bar") == "foo/bar");
        }

    BOOST_AUTO_TEST_SUITE_END() // append_

    BOOST_AUTO_TEST_SUITE(slash_assignment_)

        BOOST_AUTO_TEST_CASE(lvalue_string_)
        {
            using path::operator/=;

            string p1 = "foo";

            using T = decltype(p1 /= "bar");
            static_assert(std::same_as<T, string&>);

            p1 /= "bar";

            BOOST_TEST(p1 == "foo/bar");
        }

        BOOST_AUTO_TEST_CASE(rvalue_string_)
        {
            using path::operator/=;

            string p1 = "foo";

            using T = decltype(std::move(p1) /= "bar");
            static_assert(std::same_as<T, string&&>);

            std::move(p1) /= "bar";

            BOOST_TEST(p1 == "foo/bar");
        }

    BOOST_AUTO_TEST_SUITE_END() // slash_assignment_

BOOST_AUTO_TEST_SUITE_END() // path_

} // namespace testing
