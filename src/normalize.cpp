#include <stream9/path/normalize.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(normalize_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto s = path::normalize("");

        BOOST_TEST(s == "");
    }

    BOOST_AUTO_TEST_CASE(absolute_1_)
    {
        auto s = path::normalize("/");

        BOOST_TEST(s == "/");
    }

    BOOST_AUTO_TEST_CASE(absolute_2_)
    {
        auto s = path::normalize("/foo/");

        BOOST_TEST(s == "/foo");
    }

    BOOST_AUTO_TEST_CASE(absolute_3_)
    {
        auto s = path::normalize("/foo/bar");

        BOOST_TEST(s == "/foo/bar");
    }

    BOOST_AUTO_TEST_CASE(absolute_4_)
    {
        auto s = path::normalize("/..");

        BOOST_TEST(s == "/");
    }

    BOOST_AUTO_TEST_CASE(absolute_5_)
    {
        auto s = path::normalize("/../foo");

        BOOST_TEST(s == "/foo");
    }

    BOOST_AUTO_TEST_CASE(absolute_6_)
    {
        auto s = path::normalize("/foo/..");

        BOOST_TEST(s == "/");
    }

    BOOST_AUTO_TEST_CASE(absolute_7_)
    {
        auto s = path::normalize("/foo/bar//../baz/./../../foo");

        BOOST_TEST(s == "/foo");
    }

    BOOST_AUTO_TEST_CASE(absolute_8_)
    {
        auto s = path::normalize("/foo/../..");

        BOOST_TEST(s == "/");
    }

    BOOST_AUTO_TEST_CASE(absolute_9_)
    {
        auto s = path::normalize("/foo/../../..");

        BOOST_TEST(s == "/");
    }

    BOOST_AUTO_TEST_CASE(absolute_10_)
    {
        auto s = path::normalize("/foo/", true);

        BOOST_TEST(s == "/foo/");
    }

    BOOST_AUTO_TEST_CASE(relative_1_)
    {
        auto s = path::normalize(".");

        BOOST_TEST(s == ".");
    }

    BOOST_AUTO_TEST_CASE(relative_2_)
    {
        auto s = path::normalize("foo/");

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(relative_3_)
    {
        auto s = path::normalize("foo/bar");

        BOOST_TEST(s == "foo/bar");
    }

    BOOST_AUTO_TEST_CASE(relative_4_)
    {
        auto s = path::normalize("..");

        BOOST_TEST(s == "..");
    }

    BOOST_AUTO_TEST_CASE(relative_5_)
    {
        auto s = path::normalize("../foo");

        BOOST_TEST(s == "../foo");
    }

    BOOST_AUTO_TEST_CASE(relative_6_)
    {
        auto s = path::normalize("foo/..");

        BOOST_TEST(s == ".");
    }

    BOOST_AUTO_TEST_CASE(relative_7_)
    {
        auto s = path::normalize("foo/bar//../baz/./../../foo");

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(relative_8_)
    {
        auto s = path::normalize("foo/../..");

        BOOST_TEST(s == "..");
    }

    BOOST_AUTO_TEST_CASE(relative_9_)
    {
        auto s = path::normalize("foo/../../..");

        BOOST_TEST(s == "../..");
    }

    BOOST_AUTO_TEST_CASE(relative_10_)
    {
        auto s = path::normalize("foo/", true);

        BOOST_TEST(s == "foo/");
    }

BOOST_AUTO_TEST_SUITE_END() // normalize_

} // namespace testing
