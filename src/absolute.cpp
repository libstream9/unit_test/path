#include <stream9/path/absolute.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::path::is_absolute;
using stream9::path::to_absolute;

BOOST_AUTO_TEST_SUITE(is_absolute_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        BOOST_TEST(!is_absolute(""));
    }

    BOOST_AUTO_TEST_CASE(true_)
    {
        BOOST_TEST(is_absolute("/"));
    }

    BOOST_AUTO_TEST_CASE(false_)
    {
        BOOST_TEST(!is_absolute("foo/"));
    }

BOOST_AUTO_TEST_SUITE_END() // is_absolute_

BOOST_AUTO_TEST_SUITE(to_absolute_)

    BOOST_AUTO_TEST_CASE(empty_)
    {
        auto p1 = to_absolute("");

        BOOST_TEST(p1 == "/");
    }

    BOOST_AUTO_TEST_CASE(absolute_)
    {
        auto p1 = to_absolute("/foo");

        BOOST_TEST(p1 == "/foo");
    }

    BOOST_AUTO_TEST_CASE(relative_)
    {
        auto p1 = to_absolute("foo");

        BOOST_TEST(is_absolute(p1));
    }

BOOST_AUTO_TEST_SUITE_END() // to_absolute_

} // namespace testing
