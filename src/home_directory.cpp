#include <stream9/path/home_directory.hpp>

#include "namespace.hpp"

#include <cstdlib>

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>
#include <stream9/path/absolute.hpp>

namespace testing {

namespace test = stream9::test;

BOOST_AUTO_TEST_SUITE(home_directory_)

    BOOST_AUTO_TEST_CASE(success_)
    {
        auto const& home = path::home_directory();

        BOOST_TEST(home == std::getenv("HOME"));
        BOOST_TEST(path::is_absolute(home));
    }

    BOOST_AUTO_TEST_CASE(no_env_value_home_)
    {
        test::scoped_env e { "HOME", "" };

        BOOST_CHECK_EXCEPTION(path::home_directory(), stream9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == path::errc::invalid_environment_variable);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(env_has_relative_path_)
    {
        test::scoped_env e { "HOME", "foo" };

        BOOST_CHECK_EXCEPTION(path::home_directory(), stream9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == path::errc::invalid_environment_variable);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // home_directory_

} // namespace testing
