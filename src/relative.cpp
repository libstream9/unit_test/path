#include <stream9/path/relative.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(is_relative_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        BOOST_TEST(path::is_relative(""));
        BOOST_TEST(!path::is_relative("/"));
        BOOST_TEST(!path::is_relative("/foo"));
        BOOST_TEST(path::is_relative("foo"));
        BOOST_TEST(path::is_relative("foo/bar"));
    }

BOOST_AUTO_TEST_SUITE_END() // is_relative_

} // namespace testing
