#include <stream9/path/temporary_directory.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(temporary_directory_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        auto p = path::temporary_directory();

        BOOST_TEST(p == "/tmp");
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        st9::test::scoped_env e { "TMPDIR", "." };
        auto p = path::temporary_directory();

        BOOST_TEST(p == ".");
    }

BOOST_AUTO_TEST_SUITE_END() // temporary_directory_

} // namespace testing
