#include <stream9/path/concat.hpp>

#include "namespace.hpp"

#include <concepts>

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::string_view;
using st9::string;

BOOST_AUTO_TEST_SUITE(concat_)

    BOOST_AUTO_TEST_CASE(string_view_and_string_view_)
    {
        using path::concat;

        using T = decltype(concat("", ""));
        static_assert(std::same_as<T, string>);

        BOOST_TEST(concat("", "") == "");
        BOOST_TEST(concat("", "foo") == "foo");
        BOOST_TEST(concat("", "foo/") == "foo/");
        BOOST_TEST(concat("", "/foo") == "/foo");
        BOOST_TEST(concat("", "/foo/") == "/foo/");

        BOOST_TEST(concat("foo", "") == "foo");
        BOOST_TEST(concat("foo/", "") == "foo/");
        BOOST_TEST(concat("/foo", "") == "/foo");
        BOOST_TEST(concat("/foo/", "") == "/foo/");

        BOOST_TEST(concat("foo", "bar") == "foo/bar");
        BOOST_TEST(concat("foo/", "bar") == "foo/bar");
        BOOST_TEST(concat("/foo", "bar") == "/foo/bar");
        BOOST_TEST(concat("/foo/", "bar") == "/foo/bar");

        BOOST_TEST(concat("foo", "bar/") == "foo/bar/");
        BOOST_TEST(concat("foo/", "bar/") == "foo/bar/");
        BOOST_TEST(concat("/foo", "bar/") == "/foo/bar/");
        BOOST_TEST(concat("/foo/", "bar/") == "/foo/bar/");

        BOOST_TEST(concat("foo", "/bar") == "foo/bar");
        BOOST_TEST(concat("foo/", "/bar") == "foo/bar");
        BOOST_TEST(concat("/foo", "/bar") == "/foo/bar");
        BOOST_TEST(concat("/foo/", "/bar") == "/foo/bar");

        BOOST_TEST(concat("foo", "/bar/") == "foo/bar/");
        BOOST_TEST(concat("foo/", "/bar/") == "foo/bar/");
        BOOST_TEST(concat("/foo", "/bar/") == "/foo/bar/");
        BOOST_TEST(concat("/foo/", "/bar/") == "/foo/bar/");
    }

BOOST_AUTO_TEST_SUITE_END() // concat_

BOOST_AUTO_TEST_SUITE(slash_)

    BOOST_AUTO_TEST_CASE(string_view_)
    {
        using path::operator/;

        string_view p1 = "foo";

        using T = decltype(p1 / "bar");
        static_assert(std::same_as<T, string>);

        auto p2 = p1 / "bar";

        BOOST_TEST(p2 == "foo/bar");
    }

BOOST_AUTO_TEST_SUITE_END() // slash_

} // namespace testing
